[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date ('22) | Host          | Presenter 1     | Presenter 2     | Presenter 3      | 
| ---------- | ------------- | --------------- | --------------- | ---------------- | 
| 8/3        | Justin        | Emily Bauman    | Amelia Bauerly  | Daniel Mora      | 
| 8/17       | Taurie        | William Leidheiser | Emily Sybrant | Matej Latin     | 
| 8/31       | TBD (APAC)    | Katie Macoy     | Michael Le      |                  | 
| 9/14       | Marcel        | Nick Brandt     | Matt Nearents   | Nick Leonard     | 
| 9/28       | Rayana        | Sunjung Park    | Michael Fangman | Camellia X Yang  | 
| 10/12      | Blair         | Matt Nearents   | Becka Lippert   | Philip Joyce     | 
| 10/26      | Jacki         | Alexis Ginsberg | Andy Volpe      |                  | 
| 11/9       | Justin        | Matej Latin     | Libor Vanc      | Alexis Ginsberg  | 
| 11/23      | Taurie        | Ali Ndlovu      | Kevin Comoli    |                  | 
| 12/7       | TBD (APAC)    |                 |                 |                  | 
| 12/21      | Marcel        |                 |                 |                  | 
